﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON
{


    class Program
    {
        static void Main(string[] args)
        {
            Notes notes = new Notes();
            Console.WriteLine(notes.getText());
            Console.WriteLine(notes.getAuthor());
            Notes secondNotes = new Notes("Ovo je primjer", "Bruno Pandza", 1);
            Console.WriteLine(secondNotes.getText());
            Console.WriteLine(secondNotes.getAuthor());
            Notes thirdNotes = new Notes("Treci konstruktor", 1);
            Console.WriteLine(thirdNotes.getText());
            Console.WriteLine(thirdNotes.getAuthor());

            Notes fourthNotes = new Notes();
            Console.WriteLine(fourthNotes.Author);
            Console.WriteLine(notes);

            Console.WriteLine();

            TimeAddedNotes timeAddedNotes= new TimeAddedNotes();
            DateTime time = new DateTime(2020, 10, 2, 10, 30, 30);
            Console.WriteLine(timeAddedNotes);
            timeAddedNotes.Time = time;
            Console.WriteLine(timeAddedNotes);
            


        }
    }
}

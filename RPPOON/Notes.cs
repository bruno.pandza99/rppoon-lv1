﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON
{
    class Notes
    {
        private String text;
        private String author;
        private int levelOfImportance;
        public string getText()
        {
            return this.text;
        }
        public string getAuthor()
        {
            return this.author;
        }
        public int getLevelOfImportance()
        {
            return this.levelOfImportance;
        }
        public void setText(string text)
        {
            this.text = text;
        }
        public void setLevelOfImportance(int levelOfImportance)
        {
            this.levelOfImportance = levelOfImportance;
        }

        public string Text
        {
            get { return this.text; }
            set { this.text = value; }
        }
        public string Author
        {
            get { return this.author; }
            private set { this.author = value; }
        }
        public int LevelOfImportance
        {
            get { return this.levelOfImportance; }
            set { this.levelOfImportance = value; }
        }

        public Notes(string text,string author, int levelOfImportance)
        {
            this.text = text;
            this.author = author;
            this.levelOfImportance = levelOfImportance;
        }
        public Notes()
        {
            this.text = "Ovo je defaultni";
            this.author = "Dzontra Volta";
            this.levelOfImportance = 1;
        }
        public Notes(string text, int levelOfImportance)
        {
            this.text = text;
            this.levelOfImportance = levelOfImportance;
            this.author = "Petar Pan";
        }
        public override string ToString()
        {
            return this.Author + " je napisao: " + this.Text + " i razina prvenstva je " + this.LevelOfImportance; 
        }

    }

}

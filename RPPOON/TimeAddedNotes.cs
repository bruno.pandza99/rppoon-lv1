﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON
{
    class TimeAddedNotes : Notes
    {
        private DateTime time;

        public DateTime Time        {
            get { return this.time; }
            set { this.time = value; }
        }
        public TimeAddedNotes()
        {
            this.time = DateTime.Now;
        }
        public TimeAddedNotes(string text, string author, int levelOfImportance, DateTime time) : base(text, author, levelOfImportance)
        {
            this.time = time;
        }
        public override string ToString()
        {
            return base.ToString() + ". Vrijeme:  " + this.time;
        }
    }
}
